<?php

function wetink_css () {
	wp_enqueue_style( 'wetinkcss', get_stylesheet_directory_uri() . '/css/style.css');
}

function wetink_js () {
	wp_enqueue_script( 'wetink_js', get_stylesheet_directory_uri() . '/js/site.js', array('jquery') );
    wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() . '/js/modernizr.custom.js');

}

function add_typekit () {
    wp_enqueue_script('typekit', "//use.typekit.net/XXXX.js" );
    echo ('<script type="text/javascript">try{Typekit.load();}catch(e){}</script>');
}

function add_nivo () {
	wp_enqueue_style( 'nivo-style', get_stylesheet_directory_uri() . '/js/Nivo-Slider/nivo-slider.css' );
	wp_enqueue_script( 'nivo-script', get_stylesheet_directory_uri() . '/js/Nivo-Slider/jquery.nivo.slider.js' );
}

function add_gmaps () {
	wp_enqueue_script( 'gmaps', get_stylesheet_directory_uri() . '/js/gmaps.js' );
	// having to manually echo this to stop WordPress appending its version number since the other thing doesn't work
	echo ('<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>');
}

register_nav_menu( 'primary', __( 'Primary Menu', 'primary' ) );
add_action ('wp_enqueue_scripts', 'wetink_css');
add_action ('wp_enqueue_scripts', 'wetink_js');
//add_action ('wp_enqueue_scripts', 'add_nivo');
//add_action ('wp_enqueue_scripts', 'add_gmaps');

?>