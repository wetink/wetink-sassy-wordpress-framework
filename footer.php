<?php
/**
 * Created by PhpStorm.
 * User: Wetink
 * Date: 20/02/2014
 * Time: 16:14
 */
 ?>
<div class="clearfix"></div>

 		<?php wp_footer(); ?>
		<footer>
			<div class="wrapped">
				<div class="footer-nav">
					<?php
						wp_nav_menu();
					?>
				</div>
				<div class="whoami">
					<p>&copy; <?php echo date("Y"); ?> <?php echo get_bloginfo('name') ?>  <span class="wetink">| Website design by <a href="http://www.wetink.co.uk">Wetink</a> | Powered by <a href="http://www.purewebdevelopment.co.uk">Pure Web</a></span></p>
				</div>
			</div>
		</footer>
 	</div> <!-- End Content -->
 	</div> <!-- End footer -->
 </body>
 </html>