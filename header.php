<?php
/**
 * Created by PhpStorm.
 * User: Wetink
 * Date: 20/02/2014
 * Time: 16:14
 */
 global $post;
 ?>
<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">

	<head>
		<title><?php wp_title(); ?> | <?php bloginfo('name');?></title>
		<meta name="viewport" content="width=device-width; initial-scale=0.6;">
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<?php wp_head()?>
	</head>
	<body>
		<div class="framework">
			<header>
				<div id="logo">
					<a href="/"><img src="/wp-content/themes/wetink-2015/images/logo.png"></a>
				</div>

				<nav>
					<div class="wrapped">
						<?php
						wp_nav_menu();
						?>
					</div>
				</nav>
			</header>
			<div class="clearfix"></div>
			<div class="content">

