<? get_header()?>
<?php
$post = get_post();
?>
	<div class="wrapped post-<?=$post->ID?>">
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
			<div id="promo-image">
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
				<img src="<?php echo $image[0]; ?>">
			</div>
		<?php endif;?>
		<section class="page content-block opening">
			<h2 class="page-title"><?=$post->post_title;?></h2>
			<div class="content post-<?php echo ($post->ID)?>">
				<?php $content = apply_filters('the_content', $post->post_content);
				echo $content; ?>
			</div>
		</section>
	</div>
<? get_footer()?>