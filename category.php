<?
get_header();
$cat = get_the_category($post->id);
?>
    <div id="news-from-yard">
        <h1><?=$cat[0]->name?></h1>
        <div class="clearfix"></div>
        <div id="news-posts">
            <?  if ( have_posts() ) {
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                query_posts("cat=".$cat[0]->cat_ID."&post_type=post&paged=$paged"); ?>
                <ul>
                    <? while ( have_posts() ) {
                        the_post(); ?>
                        <li class="news-item">
                            <? if (has_post_thumbnail()) {
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
                                ?>
                                <a href="<?the_permalink()?>"><img class="featured-image" src="<?=$image[0]?>" /></a>
                            <? } ?>
                            <h4><a href="<?the_permalink()?>"><?the_title()?></a></h4>
                            <?the_content()?>
                        </li>
                    <?} // end while ?>
                </ul>
            <? } // end if ?>

            <?php if(function_exists('wp_paginate')) {
                wp_paginate();
            } else {?>
                <div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
                <div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
            <? } ?>
        </div>

        <div class="sidebar">
            <?php if ( is_active_sidebar( 'yard-sidebar' ) ) : ?>
                <ul id="sidebar">
                    <?php dynamic_sidebar( 'yard-sidebar' ); ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>


<? get_footer()?>