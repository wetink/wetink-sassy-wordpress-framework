# README #

A simple starter theme for WordPress, designed to get up and running as quickly as possible.

### Who should use this? ###

* Developers looking for the bare minimum in a WordPress theme to get up and running with a SASS powered skeleton theme.
* Version 0.1
* Requires Node and npm to install grunt

### To install ###

```
#!bash

cd yourWordPressFolder/wp-content/themes
git clone git@bitbucket.org:wetink/wordpress-sass-skeleton.git themeName
cd themeName
rm -rf .git
mv GruntFiles/* ../../..
cd ../../..
npm install

```

then Find + Replace "themeName", "authorName" and "authorEmail" with your details.

Compile your SASS with grunt sass

Watch for changes in your scss with grunt watch

### Questions ###

Contact peter@wetink.co.uk